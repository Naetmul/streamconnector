﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace StreamConnector
{
    public class Player
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public bool Exists { get; set; }
    }

    public class PlayerInfo
    {
        public static List<Player> PlayerList { get; set; }

        /* A static constructor is used to initialize any static data,
         * or to perform a particular action that needs to be performed once only.
         * It is called automatically before the first instance is created or any static members are referenced.
         */
        static PlayerInfo()
        {
            PlayerList = new List<Player>();

            PlayerList.Add(new Player
            {
                    ID = "PotPlayerMini32",
                    Name = "Daum PotPlayer Mini (32-bit)",
                    Path = Path.Combine(PathUtility.ProgramFilesX86Path, @"DAUM\PotPlayer\PotPlayerMini.exe")
            });
            if (PathUtility.ProgramFilesX64Path != null) {
                PlayerList.Add(new Player
                {
                    ID = "PotPlayerMini64",
                    Name = "Daum PotPlayer Mini (64-bit)",
                    Path = Path.Combine(PathUtility.ProgramFilesX64Path, @"DAUM\PotPlayer\PotPlayerMini64.exe")
                });
            }
            PlayerList.Add(new Player
            {
                ID = "PotPlayer32",
                Name = "Daum PotPlayer Live (32-bit)",
                Path = Path.Combine(PathUtility.ProgramFilesX86Path, @"DAUM\PotPlayer\PotPlayer.exe")
            });
            if (PathUtility.ProgramFilesX64Path != null) {
                PlayerList.Add(new Player
                {
                    ID = "PotPlayer64",
                    Name = "Daum PotPlayer Live (64-bit)",
                    Path = Path.Combine(PathUtility.ProgramFilesX64Path, @"DAUM\PotPlayer\PotPlayer64.exe")
                });
            }

            PlayerList = PlayerList.Select(player =>
            {
                player.Exists = File.Exists(player.Path);
                return player;
            }).ToList();
        }
    }

    [ValueConversion(typeof(bool), typeof(string))]
    public class ExistsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value)
            {
                return null;
            }
            else
            {
                return TextDecorations.Strikethrough;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
