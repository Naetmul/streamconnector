﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StreamConnector
{
    public class Channel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

    public class ChannelInfo
    {
        public static string ChannelXmlPath
        {
            get
            {
                return Path.Combine(PathUtility.LocalAppConfigFolderPath, "Channel.xml");
            }
        }

        public static List<Channel> ChannelList { get; set; }

        static ChannelInfo()
        {
            ChannelList = LoadFromXml();
        }

        public static List<Channel> LoadFromXml() {
            List<Channel> channels = new List<Channel>();

            if (File.Exists(ChannelXmlPath))
            {
                XDocument xDoc = XDocument.Load(ChannelXmlPath);

                XElement xChannels = xDoc.Element("Channels");

                foreach (XElement xChannel in xChannels.Elements("Channel"))
                {
                    channels.Add(new Channel { Name = xChannel.Element("Name").Value, Url = xChannel.Element("Url").Value });
                }
            }

            return channels;
        }

        public static void SaveToXml(List<Channel> channels)
        {
            XDocument xDoc = new XDocument();

            XElement xChannels = new XElement("Channels");

            foreach (Channel channel in channels)
            {
                XElement xChannel = new XElement("Channel");
                xChannel.Add(new XElement("Name", channel.Name));
                xChannel.Add(new XElement("Url", channel.Url));

                xChannels.Add(xChannel);
            }

            xDoc.Add(xChannels);

            if (!Directory.Exists(PathUtility.LocalAppConfigFolderPath))
            {
                Directory.CreateDirectory(PathUtility.LocalAppConfigFolderPath);
            }

            xDoc.Save(ChannelXmlPath);
        }
    }
}
