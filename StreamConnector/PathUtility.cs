﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamConnector
{
    public static class PathUtility
    {
        public static string LocalAppConfigFilePath
        {
            get
            {
                return ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            }
        }

        public static string LocalAppConfigFolderPath
        {
            get
            {
                return Path.GetDirectoryName(LocalAppConfigFilePath);
            }
        }

        public static string ProgramFilesX86Path
        {
            get
            {
                if (!Environment.Is64BitOperatingSystem)
                {
                    /* 32-bit OS */
                    return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                }
                else
                {
                    /* 64-bit OS */
                    return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                }
            }
        }

        public static string ProgramFilesX64Path
        {
            get
            {
                if (!Environment.Is64BitOperatingSystem)
                {
                    /* 32-bit OS */
                    return null;
                }
                else
                {
                    /* 64-bit OS */
                    return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                }
            }
        }
    }
}
