﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace StreamConnector
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static int VersionCode
        {
            get
            {
                return 1;
            }
        }

        // Executed when the app starts.
        private void OnStartup(object sender, StartupEventArgs e)
        {
            /* If this is the first run,
             * then reset the Settings to its default value.
             */
            if (StreamConnector.Properties.Settings.Default.IsFirstRun)
            {
                // Channel
                StreamConnector.Properties.Settings.Default.ChannelUrl = @"http://www.twitch.tv/wcs";

                // Resolution
                StreamConnector.Properties.Settings.Default.Resolution = "source";

                // Player
                Player player = PlayerInfo.PlayerList.FirstOrDefault(p => p.Exists); // Can be null
                StreamConnector.Properties.Settings.Default.PlayerPath = (player != null) ? player.Path : String.Empty;
            }
        }

        // Executed when the app ends.
        private void OnExit(object sender, ExitEventArgs e)
        {
            if (StreamConnector.Properties.Settings.Default.IsFirstRun)
            {
                StreamConnector.Properties.Settings.Default.IsFirstRun = false;
            }

            StreamConnector.Properties.Settings.Default.Save();
        }

    }
}
