﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamConnector
{
    public class Resolution
    {
        public string Title { get; set; }
        public string Argument { get; set; }
    }

    public class ResolutionInfo
    {
        private static List<Resolution> resolutionList = new List<Resolution>()
        {
            new Resolution { Title = "원본 (Source)", Argument = "source" },
            new Resolution { Title = "높음 (High)", Argument = "high" },
            new Resolution { Title = "중간 (Medium)", Argument = "medium" },
            new Resolution { Title = "낮음 (Low)", Argument = "low" },
            new Resolution { Title = "피처폰 수준 (Mobile)", Argument = "mobile" },
            new Resolution { Title = "영상 없이 소리만 (Audio)", Argument = "audio" },
            new Resolution { Title = "오류 대비용 1 (Best)", Argument = "best" },
            new Resolution { Title = "오류 대비용 2 (Worst)", Argument = "worst" },
        };

        public static List<Resolution> ResolutionList
        {
            get
            {
                return resolutionList;
            }
        }
    }
}
