﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StreamConnector
{
    /// <summary>
    /// Interaction logic for ChannelEditWindow.xaml
    /// </summary>
    public partial class ChannelEditWindow : Window
    {
        public List<Channel> ChannelList { get; set; }

        public ChannelEditWindow()
        {
            InitializeComponent();

            ChannelList = ChannelInfo.LoadFromXml();
            channelListView.ItemsSource = ChannelList;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            ChannelList.Add(new Channel { Name = nameText.Text, Url = urlText.Text });
            channelListView.Items.Refresh();
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (channelListView.SelectedItem is Channel)
            {
                ChannelList.Remove((Channel)channelListView.SelectedItem);
                channelListView.Items.Refresh();
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            ChannelInfo.SaveToXml(ChannelList);
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void channelListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Channel channel = channelListView.SelectedItem as Channel;

            if (channel != null)
            {
                nameText.Text = channel.Name;
                urlText.Text = channel.Url;
            }
        }
    }
}
