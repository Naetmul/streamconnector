﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace StreamConnector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            foreach (Resolution resolution in ResolutionInfo.ResolutionList)
            {
                RadioButton radioButton = new RadioButton();
                radioButton.Content = resolution.Title;
                radioButton.Tag = resolution;
                radioButton.Checked += (sender, e) => StreamConnector.Properties.Settings.Default.Resolution = ((Resolution)((RadioButton)sender).Tag).Argument;
                resolutionPanel.Children.Add(radioButton);

                if (resolution.Argument == StreamConnector.Properties.Settings.Default.Resolution)
                {
                    radioButton.IsChecked = true;
                }
            }

            if (StreamConnector.Properties.Settings.Default.IsFirstRun)
            {
                MessageBox.Show("Livestreamer를 설치하지 않았다면 '도구' 메뉴에서 'Livestreamer 다운로드'를 눌러 설치하세요.");
            }
        }

        #region Button Events

        private void EditChannelList_Click(object sender, RoutedEventArgs e)
        {
            ChannelEditWindow cew = new ChannelEditWindow();
            cew.ShowDialog();

            ChannelInfo.ChannelList = ChannelInfo.LoadFromXml();
            /* This is another object; i.e., new reference, so Items.Refresh() does not work. */
            channelListView.ItemsSource = ChannelInfo.ChannelList;
        }

        private void PlayerPathBrowse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = "Executable Files|*.exe|All Files|*.*";
            try
            {
                dlg.InitialDirectory = System.IO.Path.GetDirectoryName(this.playerPathText.Text);
                dlg.FileName = System.IO.Path.GetFileName(this.playerPathText.Text);
                dlg.DefaultExt = ".exe";
            }
            catch { }
            
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                this.playerPathText.Text = dlg.FileName;
            }
        }

        private void Execute_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(playerPathText.Text))
            {
                MessageBox.Show(playerPathText.Text + " 파일을 찾을 수 없습니다. 플레이어 경로가 올바른지 확인해 주세요.");
                return;
            }

            Resolution resolution = ResolutionInfo.ResolutionList[0];
            foreach (RadioButton radio in resolutionPanel.Children) {
                if (radio.IsChecked.HasValue && radio.IsChecked.Value)
                {
                    resolution = radio.Tag as Resolution;
                    break;
                }
            }

            Process process = new Process();

            process.StartInfo.FileName = "livestreamer";
            process.StartInfo.Arguments = channelUrlText.Text + " " + resolution.Argument + @" --player """ + playerPathText.Text + @"""";

            /* If the UseShellExecute property is true or the UserName and Password properties are not null,
             * the CreateNoWindow property value is ignored and a new window is created. */
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;

            /* 이 이벤트는 StandardOutput에 대한 비동기 읽기 작업 동안에 활성화됩니다.
             * 비동기 읽기 작업을 시작하려면 Process의 StandardOutput 스트림을 리디렉션하고,
             * 이벤트 처리기를 OutputDataReceived 이벤트에 추가한 다음 BeginOutputReadLine을 호출해야 합니다. */
            process.StartInfo.RedirectStandardOutput = true;
            process.OutputDataReceived += (sendingProcess, outLine) => Dispatcher.Invoke(() => this.outputText.Text += outLine.Data + "\n");

            /*  Exited 이벤트는 연결된 프로세스가 종료된 것을 나타냅니다. 이 이벤트의 발생은 프로세스가 종료(중단)되었거나 닫힌 것을 의미합니다.
             * 이 이벤트는 EnableRaisingEvents 속성의 값이 true인 경우에만 발생할 수 있습니다. */
            process.EnableRaisingEvents = true;
            process.Exited += (sendingProcess, data) => Dispatcher.Invoke(() => this.outputText.Text += "Livestreamer가 종료되었습니다." + "\n");

            try
            {
                process.Start();
                process.BeginOutputReadLine();
            }
            catch
            {
                MessageBox.Show("Livestreamer를 찾을 수 없습니다. 설치하지 않았다면 도구 메뉴를 눌러 설치해 주세요.");
            }
        }

        #endregion Button Events

        #region Menus

        private void DownloadLivestreamer_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Livestreamer 홈페이지에서 Windows Binaries Installer를 눌러 다운로드 받아 설치하세요.");
            Process.Start("http://livestreamer.readthedocs.org/en/latest/install.html#windows-binaries");
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private async void Update_Click(object sender, RoutedEventArgs e)
        {
            Task<XDocument> xLoadTask = Task<XDocument>.Run(() =>
            {
                XDocument xDocTemp = null;
                try
                {
                    xDocTemp = XDocument.Load("https://bitbucket.org/Naetmul/streamconnector/downloads/UpdateInfo.xml");
                }
                catch
                {
                    MessageBox.Show("업데이트 정보를 확인할 수 없습니다. 인터넷에 연결되어 있지 않거나 서버에 문제가 있을 수 있습니다.");
                }
                return xDocTemp;
            });

            XDocument xDoc = await xLoadTask;
            if (xDoc == null) return;

            XElement xUpdate = xDoc.Element("Update");
            XElement xLatest = xUpdate.Element("Latest");

            int versionCode = Convert.ToInt32(xLatest.Element("VersionCode").Value);
            string url = xLatest.Element("Url").Value;

            if (versionCode > App.VersionCode)
            {
                MessageBoxResult result = MessageBox.Show("새 버전이 있습니다. 새 버전을 다운로드 받으시겠습니까?", "새 버전 확인", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (result == MessageBoxResult.Yes)
                {
                    Process.Start(url);
                }
            }
            else
            {
                MessageBox.Show("현재 버전이 최신 버전입니다.");
            }
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }

        #endregion Menus

        private void channelListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Channel channel = channelListView.SelectedItem as Channel;

            if (channel != null)
            {
                // TextView를 바꾸면 Settings이 바뀌지 않음.
                StreamConnector.Properties.Settings.Default.ChannelUrl = channel.Url;
            }
        }

        private void playerListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StreamConnector.Properties.Settings.Default.PlayerPath = playerListBox.SelectedItem is Player ? ((Player)playerListBox.SelectedItem).Path : "";
        }

        private void outputText_TextChanged(object sender, TextChangedEventArgs e)
        {
            outputScroll.ScrollToBottom();
        }
    }
}
