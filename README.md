# README #

**StreamConnector** is a C# Windows application that enables you to watch [Twitch.tv](http://www.twitch.tv) via [Daum PotPlayer](http://potplayer.daum.net/).
StreamConnector requires [Livestreamer](http://livestreamer.readthedocs.org) to run properly.
StreamConnector is open-sourced, with Apache 2.0 License.
Currently, the only Korean version is available.

### Install ###
* OS Requirement: Microsoft Windows that can run Daum PotPlayer and .NET Framework 4.5.1. (Windows 7 or newer versions are recommended.)
* Download Link: You can download the installer from the [Downloads section](https://bitbucket.org/Naetmul/streamconnector/downloads), or directly from [this link](https://bitbucket.org/Naetmul/streamconnector/downloads/StreamConnector.zip).
* The latest version is 1.1.

### How to Install ###
* Unzip the *StreamConnector.zip* file.
* Run *Setup.exe*.
* You will need Livestreamer if you haven't installed it already. You can install it if you run StreamConnector.

### Source Code ###
The project was developed with Microsoft Visual Studio 2013.

### Licenses ###
This project follows Apache 2.0 License.

This project uses Crystal Project Icons, which follows LGPL License.